[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

# grasp_synthesis

This is a ROS package to synthesize grasps using tsgrasp.

[paper](https://research.engr.oregonstate.edu/rdml/sites/research.engr.oregonstate.edu.rdml/files/icra23_0859_fi_0.pdf) | [ICRA Video](https://youtu.be/E31_ryG2xGY) | [Presentation](https://www.youtube.com/watch?v=JBEWkCMrQKs)

[![Demo Video](https://img.youtube.com/vi/JBEWkCMrQKs/0.jpg)](https://youtu.be/JBEWkCMrQKs)


## About
This repo contains utilities for synthesizing grasps from an input streaming point cloud.

### Grasp Synthesis Node
Grasp Synthesis as a node is run inside a docker container, and the number of scripts is a bit annoying. The flow is shown below:

![node_flow](assets/grasp_synth_flow.png)

The pipeline is setup to function in two modes. Both modes rely on proper configuration of the [metadata.yaml](grasp_synthesis/models/tsgrasp_scene_1_frame/metadata.yaml) file for topic remapping and network parameters.

ROS-specific params:
```yaml
ckpt_path: src/grasp_synthesis/tsgrasp/models/tsgrasp_scene_1_frame/model.ckpt # relative to pkg_root
pc_topic: /point_cloud_cropper/cropped_pc
queue_len: 1
outlier_threshold: 0.00005
pts_per_frame: 45000
```

The launch file looks for a few args:
```xml
<param name="model_metadata" value="models/tsgrasp_scene_1_frame/metadata.yaml"/>
<param name="verbose" value="True"/>
```

The node subscribes to the `pc_topic` from the yaml. It publishes a constant detection output to `grasps_unfiltered` and `grasp_confidences_pc`. The synthesis happens on the ENTIRE pointcloud. Cropping should be done prior to passing the cloud into the node.

The node also runs a `get_grasps` actionlib server that takes as input a `GetGraspsGoal` msg (containing a required pointcloud and an optional bounding box) and the server returns a `GetGraspsResult` containing a pointcloud array. This is done for speed, as generating 10k Grasp messages was taking ~0.25s and creating a performance bottleneck. Although a PointCloud2 is less semantically meaningful than Grasps/Grasp messages, it was a necessary to achieve real-time performance. We have not determined exactly what is causing the slowdown with message generation in rospy.

### Grasp Synthesis Output
For speed, the output of the synthesis network is packed into a PointCloud2 array. This is because there are ~10,000 grasp results from the synthesis network and creating a Grasp message for all of those points takes a very long time.

The output is a tuple of pointclouds. The first is an array containing 4x4 pose arrays (shape= (n, 4, 4)), confidences (shape= (n, 1)) and widths (shape= (n, 1)):

```python
grasps_array = np.zeros(
            (npoints,),
            dtype=[
                ("grasps", np.float32, (4, 4)),
                ("confidences", np.float32),
                ("widths", np.float32),
            ],
        )
```

The second is a typical points array with colors mapped by confidencse:
```python
cm_array = np.zeros(
            (npoints,),
            dtype=[
                ("x", np.float32),
                ("y", np.float32),
                ("z", np.float32),
                ("r", np.float32),
                ("g", np.float32),
                ("b", np.float32),
                ("a", np.float32),
            ],
        )
```


## Testing
Testing grasp synthesis works standalone:

```bash
./test/test_node_docker.sh
...
[INFO] [1710882559.459090]: Starting Grasp Synthesis Node.
test_detection (__main__.TestGraspSynthesis)
Test single detection ... ok
test_detection_speed (__main__.TestGraspSynthesis)
Test detection speed ... Average Inference Time/Frequency: 0.044s/22.857 Hz
ok
test_memory_consumption (__main__.TestGraspSynthesis)
Test- Memory Consumption ... GPU used 123.697 MB
ok
[INFO] [1710882561.723946]: Stopping grasp_synthesis_node.test..

----------------------------------------------------------------------
Ran 3 tests in 6.739s

OK
```

## Installation and Usage (Docker)
* Ensure you have the tsgrasp repo from the repos file `
* This is pretty hefty, so to simplify rebuilds, the base image only contains the requirements, but not the actual scripts/models
* Two options - To build the image your self:
  * Ensure you have downloaded the checkpoint model you'd like to run from [here](https://drive.google.com/drive/folders/1UJ_D84t963Z94wAed2NlYJ6Fg8MPRwJw?usp=drive_link). We use [tsgrasp_scene_1_frame](https://drive.google.com/drive/folders/1tVthHAhcvxSxh_pbByTjcRMrzuYbdoF4?usp=drive_link)
  * Inside the `/docker/` folder, run, `sudo ./build.sh`
  * This image is about 20gb and takes ages. You've been warned.
* Pull from docker:
  * `docker pull registry.gitlab.com/apl-ocean-engineering/raven/manipulation/grasp_synthesis/grasp_synthesis:base`
  * `docker pull registry.gitlab.com/apl-ocean-engineering/raven/manipulation/grasp_synthesis/grasp_synthesis:node`

Using docker means you don't need to manually install difficult dependencies such as Minkowski Engine. If you would like to install these manually, see [docker/Dockerfile](docker/Dockerfile) to view installation steps.

## Downloading weights
When using the prebuilt Docker image, it isn't necessary to download the weights. However, when compiling the docker image or running locally, you can download the weights from Google Drive [here](https://drive.google.com/drive/folders/1UJ_D84t963Z94wAed2NlYJ6Fg8MPRwJw?usp=drive_link).

## Switching Between Models
| :exclamation:  Not Currently Supported!  |
|-----------------------------------------|
Currently, this repo only supports single-frame inferencing. Instructions for the old repo are included for reference.

1. Copy the configuration string `cfg_str` from `nn/ckpts/tsgrasp_scene_4_random_yaw/README.md` into `nn/load_model.py`. This tells the prediction script where to find the new model weights and how to configure the model.
1. Change `queue_len` in `predict_grasps.py` to 4.
1. Run the grasp prediction node again. Your changes will be bind-mounted into the container when Docker is invoked in `nodes/predict_grasps_docker.sh`.

## Configuring the Grasp Prediction Pipeline
The function `find_grasps()` in `nodes/predict_grasps.py` contains several pre- and post-processing steps beyond model inference. In order, the preprocessing steps are:
- Randomly downsample the points with uniform probability.
- Remove points that have few neighbors.
- If multiple point clouds are processed at once, transform all points to the latest camera frame, as done during training.

After inferring the grasp poses, confidences, and grasp widths using TSGrasp, the postprocessing steps are:
- Rotate grasps by 180 degrees around the gripper axis if their y-axis is facing downward. This prevented our gripper, which rotated very slowly, from continually rotating during consecutive trials.
- Translate the grasps along the gripper axis to accommodate the the length of the gripper.
- Publish the grasps as a pointcloud
- Publish an additional PointCloud2 to visualize the grasp confidences of the point cloud.

The recommended way to enable, disable, or change the parameters of any step in this pipeline is to modify `predict_grasps.py`. When you run the grasp synthesis node using `grasp_synthesis_node_docker.sh`, the modified file will be used by the container.

## Cite
```
@inproceedings{tsgrasp2023,
  title={Real-Time Generative Grasping with Spatio-temporal Sparse Convolution},
  author={Player, Timothy R and Chang, Dongsik and Fuxin, Li and Hollinger, Geoffrey A},
  year = {2023},
  booktitle = {Proc. IEEE International Conference on Robotics and Automation},
  address = {London, UK}
}
```

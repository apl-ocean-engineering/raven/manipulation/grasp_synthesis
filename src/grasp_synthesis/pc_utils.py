"""
Collection of utility functions used in grasp_synthesis
for pointcloud handling

Author: Marc Micatka
University of Washington-APL 2024
"""

import numpy as np

from raven_manip_msgs.msg import BoundingBox3D


def cropped_pc_from_bbox(pts: np.array, bbox: BoundingBox3D):
    """
    Bounds a point cloud to a specified world boundary.

    Args:
      pts: A numpy array of shape (N, 3) representing the point cloud.
      bbox: BoundingBox3D

    Returns:
      A numpy array of shape (M, 3) representing the bounded point cloud,
      or None if no points remain within the bounds.
    """
    # Calculate min and max points
    center = [
        bbox.center.position.x,
        bbox.center.position.y,
        bbox.center.position.z,
    ]
    size = [bbox.size[0], bbox.size[1], bbox.size[2]]
    xyz_min = np.array([center[i] - size[i] / 2 for i in range(3)])
    xyz_max = np.array([center[i] + size[i] / 2 for i in range(3)])

    # Apply boolean mask to filter points within bounds
    valid = (
        (pts[:, 0] >= xyz_min[0])
        & (pts[:, 0] <= xyz_max[0])
        & (pts[:, 1] >= xyz_min[1])
        & (pts[:, 1] <= xyz_max[1])
        & (pts[:, 2] >= xyz_min[2])
        & (pts[:, 2] <= xyz_max[2])
    )
    pts_filtered = pts[valid]

    # Check if any points remain
    if not np.any(valid):
        return None

    return pts_filtered

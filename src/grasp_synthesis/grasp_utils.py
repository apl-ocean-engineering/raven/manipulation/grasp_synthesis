"""
Collection of utility functions used in grasp_synthesis
for grasp message handling/generation

Author: Marc Micatka
University of Washington-APL 2024
"""

import rospy
import yaml


def model_metadata_from_yaml(yaml_file_path: str) -> dict:
    """
    Gets config dictionary from yaml file

    Args:
        yaml_file_path (str): path (relative) to yaml config file

    Returns:
        metadata (dict): metadata dictionary
    """
    metadata = {}

    try:
        with open(yaml_file_path, "r", encoding="utf-8") as stream:
            metadata = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        rospy.logwarn(exc)
    return metadata


def is_message_all_zeros(message):
    """
    Checks if all fields in a ROS message are zero.

    Args:
        message: A ROS message instance.

    Returns:
        True if all fields in the message are zero, False otherwise.
    """
    # check center:
    center_all_zeros = (
        message.center.position.x == 0.0
        and message.center.position.y == 0.0
        and message.center.position.z == 0.0
        and message.center.orientation.x == 0.0
        and message.center.orientation.y == 0.0
        and message.center.orientation.z == 0.0
        and message.center.orientation.w == 0.0
    )
    # check size
    size_all_zeros = (
        message.size.x == 0.0 and message.size.y == 0.0 and message.size.z == 0.0
    )
    return center_all_zeros and size_all_zeros

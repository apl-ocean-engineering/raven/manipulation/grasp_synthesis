import os
import signal
import subprocess
import time
import unittest
from typing import Optional

import numpy as np
import rospkg
import rospy
import torch
import yaml

from grasp_synthesis.tsgrasp.predict_grasps import GraspPredictor

# Initialize the ROS package manager
rospack = rospkg.RosPack()
pkg_root = rospack.get_path("grasp_synthesis")

REQUIRED_POSE_CNT = 100
REQUIRED_INFERENCE_TIME = 1 / 7  # 7 Hz
MAX_MEMORY_CONSUMPTION = 1000  # MB


def model_metadata_from_yaml(yaml_file_path: str) -> dict:
    """
    Gets config dictionary from yaml file

    Args:
        yaml_file_path (str): path (relative) to yaml config file

    Returns:
        metadata (dict): metadata dictionary
    """
    metadata = {}

    try:
        with open(yaml_file_path, "r", encoding="utf-8") as stream:
            metadata = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        rospy.logwarn(exc)
    return metadata


def load_pc_data() -> Optional[np.array]:
    """
    Load point cloud from file

    Returns:
        pc_data (np.array): numpy array of pc data
    """
    pc_path = os.path.join(pkg_root, "test/data/pc_data.npy")
    pc_data: Optional[np.array] = None
    try:
        with open(pc_path, "rb") as f:
            pc_data = np.load(f)
    except Exception:
        print("Error reading file!")
    return pc_data


class TestGraspSynthesis(unittest.TestCase):
    """Test Grasp Synthesis"""

    @classmethod
    def setUpClass(cls):
        # Start the grasp_synthesis_node in a separate thread
        subprocess.Popen("roscore")
        rospy.sleep(1)

        rospy.init_node("test_get_grasps_service", anonymous=True)
        rospy.set_param("/grasp_synthesis_node/service_mode", True)
        rosrun_command = [
            "rosrun",
            "--prefix",
            f"{os.environ['NN_CONDA_PATH']}",
            "grasp_synthesis",
            "grasp_synthesis_node.py",
        ]
        cls.node_process = subprocess.Popen(rosrun_command)
        rospy.sleep(2)  # Allow some time for the node to start (adjust as needed)

        # Set up some initial state here
        cls.pc_data = load_pc_data()
        cls.detector = cls.load_detector()

    @classmethod
    def tearDownClass(cls):
        """
        Stop the grasp_synthesis_node subprocess after running the tests
        """
        rospy.loginfo("Stopping grasp_synthesis_node...")
        cls.node_process.send_signal(signal.SIGINT)
        cls.node_process.wait()

    @classmethod
    def load_detector(cls):
        """
        Load Grasp Synthesis Detector
        """
        yaml_file_path = (
            "src/grasp_synthesis/tsgrasp/models/tsgrasp_scene_1_frame/metadata.yaml"
        )
        model_metadata = model_metadata_from_yaml(
            os.path.join(pkg_root, yaml_file_path)
        )

        return GraspPredictor(model_metadata, False, pkg_root)

    def test_detection(self):
        """
        Test single detection
        """
        pygrasps = None
        points_arr = None
        pygrasps, points_arr = self.detector.detect(self.pc_data)

        self.assertTrue(pygrasps is not None)
        self.assertTrue(points_arr is not None)

        passing_detections = len(pygrasps.grasps) >= REQUIRED_POSE_CNT
        self.assertTrue(
            passing_detections,
            f"Found {pygrasps.grasps} poses. Required: {REQUIRED_POSE_CNT} poses.",
        )

    def test_detection_speed(self):
        """
        Test detection speed
        """
        inference_speeds = []
        for _ in range(10):
            t0 = time.time()
            self.detector.detect(self.pc_data)
            t1 = time.time()
            inference_speeds.append(t1 - t0)
        speeds = np.array(inference_speeds)
        passing_inference_time = speeds.mean() < REQUIRED_INFERENCE_TIME
        print(
            f"Average Inference Time/Frequency: {speeds.mean():.3f}s/{(1/speeds.mean()):.3f} Hz"
        )
        self.assertTrue(
            passing_inference_time,
            f"Average Inference Time: {speeds.mean():.3f} s. Passing requires inference time of {REQUIRED_INFERENCE_TIME:.3f} s",
        )

    def test_memory_consumption(self):
        """
        Test- Memory Consumption
        """
        try:
            torch.cuda.reset_peak_memory_stats(device=None)
            _, _ = self.detector.detect(self.pc_data)
            max_memory_used = torch.cuda.max_memory_allocated(device=None) / 1e6
            print(f"GPU used {max_memory_used:.3f} MB")
            self.assertTrue(max_memory_used < MAX_MEMORY_CONSUMPTION)

        except AssertionError as ex:
            self.fail(f"Failed! - {ex}")


def suite():
    """Create test suite for all test cases"""
    test_suite = unittest.TestSuite()
    test_loader = unittest.TestLoader()
    test_suite.addTest(test_loader.loadTestsFromTestCase(TestGraspSynthesis))
    return test_suite


if __name__ == "__main__":
    unittest.TextTestRunner(verbosity=2).run(suite())

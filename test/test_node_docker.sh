#!/bin/bash

# Run grasp prediction in a Docker image with predict_grasps.py bind-mounted in.
# this script expects the following folder path:
# . (grasp_synthesis)
# └── nodes
#     └── grasp_synthesis_node.sh
#    SCRIPT_DIR: /home/mmicatka/Documents/raven_manipulation/src/grasp_synthesis/nodes
# docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

docker run --rm -it \
    --gpus all \
    --network host \
    -v $SCRIPT_DIR/../src:/grasp_synth/grasp_synth_ws/src/grasp_synthesis/src \
    -v $SCRIPT_DIR/../test:/grasp_synth/grasp_synth_ws/src/grasp_synthesis/test \
    -v $SCRIPT_DIR/../nodes/grasp_synthesis_node.py:/grasp_synth/grasp_synth_ws/src/grasp_synthesis/nodes/grasp_synthesis_node.py \
    grasp_synth:latest \
    /bin/bash -c "source /grasp_synth/grasp_synth_ws/devel/setup.bash && \
    \${NN_CONDA_PATH} grasp_synth_ws/src/grasp_synthesis/test/test_synthesis.py"

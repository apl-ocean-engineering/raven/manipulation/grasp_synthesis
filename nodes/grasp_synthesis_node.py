"""
Grasp Synthesis Node

Author: Marc Micatka
University of Washington-APL 2023
"""

import os
import threading
import time
from typing import Optional

import actionlib
import numpy as np
import rospkg
import rospy
import torch
from sensor_msgs.msg import PointCloud2

from grasp_synthesis.grasp_utils import is_message_all_zeros, model_metadata_from_yaml
from grasp_synthesis.pc_utils import cropped_pc_from_bbox
from grasp_synthesis.tsgrasp.predict_grasps import GraspPredictor
from raven_manip_msgs.msg import GetGraspsAction, GetGraspsGoal, GetGraspsResult
from ros_numpy.point_cloud2 import array_to_pointcloud2, pointcloud2_to_xyz_array

# Initialize the ROS package manager
rospack = rospkg.RosPack()
pkg_root = rospack.get_path("grasp_synthesis")


class GraspSynthesis:
    """
    Abstract GraspSynthesis node

    This node subscribes to pointcloud topic and a BoundingBox3D topic
    Publishes grasps from service call
    """

    def __init__(self) -> None:
        # Hacky solution to allow for param getting inside docker
        # The issue is that the node is "rosrun" in a script launched by the docker run script
        # This means the namespace inside the node is "/" and not "grasp_synthesis_node" which is endlessly frustrating.
        # Absolute parameters will still be found correctly but relative/private params will not
        try:
            node_name = rospy.get_name()
            yaml_file_path = rospy.get_param(
                f"{node_name}/model_metadata",
                "src/grasp_synthesis/tsgrasp/models/tsgrasp_scene_1_frame/metadata.yaml",
            )
            self.verbose = rospy.get_param(f"{node_name}/verbose", False)
            model_metadata = model_metadata_from_yaml(
                os.path.join(pkg_root, yaml_file_path)
            )
        except KeyError:
            rospy.logerr("Could not get params!")
            return

        # Member vars:
        self.device = torch.device("cuda")
        self._current_pc_msg: Optional[PointCloud2] = None
        self._current_pc: Optional[np.array] = None
        self._current_pc_tf: Optional[np.array] = None
        self._has_new_pc_msg = False
        self._rate = rospy.Rate(20)
        self._pc_lock = threading.Lock()
        self._warmup_run = False

        # Initiate Grasp Synthesis
        self.grasp_predictor = GraspPredictor(model_metadata, self.verbose, pkg_root)

        # Start actionlib_server for param setting:
        self._server = actionlib.SimpleActionServer(
            "/get_grasps",
            GetGraspsAction,
            execute_cb=self.get_grasps_callback,
            auto_start=False,
        )
        self._server.start()

        if model_metadata is None:
            rospy.logerr("No Metadata Loaded!")
        try:
            pc_topic = model_metadata["pc_topic"]

        except KeyError as ex:
            rospy.logerr(f"Key Error: {ex}")

        if self.verbose:
            rospy.loginfo(f"Listening for point clouds from {pc_topic}")

        # Setup Subscribers
        self.pc_sub = rospy.Subscriber(
            pc_topic,
            PointCloud2,
            self.pc_callback,
            queue_size=10,
        )
        # Setup Publishers
        self.grasps_pc_pub = rospy.Publisher(
            "~grasps_unfiltered", PointCloud2, queue_size=10
        )
        self.conf_pc_pub = rospy.Publisher(
            "~grasp_confidences_pc", PointCloud2, queue_size=10
        )

        # Call detect in a loop
        self.run_in_loop()

    def get_grasps_callback(self, goal: GetGraspsGoal):
        """
        Callback for the GetGrasps action.

        Args:
            goal (GetGraspsGoal): The action goal containing the point cloud and bounding box
        """

        rospy.loginfo("Received GetGraspsAction...")

        grasps_array = None
        result = GetGraspsResult()

        try:
            pc_data = pointcloud2_to_xyz_array(goal.cloud).reshape(-1, 3)
            if not is_message_all_zeros(goal.bbox):
                pc_data = cropped_pc_from_bbox(pc_data, goal.bbox)

            if len(pc_data) > 0:
                t0 = time.time()
                # grasps array contains the grasp poses, confidences, and widths.
                grasps_array, _ = self.grasp_predictor.detect(pc_data)
                t1 = time.time()

                if self.verbose:
                    rospy.loginfo(
                        f"Found {len(grasps_array)} grasps in {1000*(t1 - t0):.2f} ms"
                    )

                if grasps_array is not None:
                    try:
                        grasps_pc = array_to_pointcloud2(grasps_array)
                        grasps_pc.header = goal.cloud.header
                        result.grasps_cloud = grasps_pc
                        self._server.set_succeeded(result)
                    except Exception as ex:
                        rospy.logerr(f"Failed to get grasps from pygrasps: {ex}")
                        self._server.set_aborted(text="Failed to get grasps!")

            else:
                self._server.set_aborted(text="No points in pointcloud!")
        except Exception as ex:
            rospy.logerr(f"Failed to get grasps: {ex}")
            self._server.set_aborted(text="Failed to get grasps!")

    def pc_callback(self, pc_msg: PointCloud2) -> None:
        """
        Store incoming point cloud as numpy array

        Args:
            msg (PointCloud2): incoming point cloud
        """
        with self._pc_lock:
            self._current_pc_msg = pc_msg
            self._has_new_pc_msg = True

            # Store PC as numpy array:
            self._current_pc = pointcloud2_to_xyz_array(pc_msg).reshape(-1, 3)

        # The first run of of torch model takes ages to initialize.
        # Speed up subsequent runs by doing a fake run
        if not self._warmup_run:
            grasps, pc_confs = self.grasp_predictor.detect(self._current_pc)
            if grasps is not None and pc_confs is not None:
                self._warmup_run = True
                if self.verbose:
                    rospy.loginfo("Done running warmup detection!")

    def run_in_loop(self) -> None:
        """
        Run the synthesis in a loop
        """
        grasps_pc: Optional[PointCloud2] = None
        confidences_pc: Optional[PointCloud2] = None

        while not rospy.is_shutdown():
            with self._pc_lock:
                if self._has_new_pc_msg and self._current_pc_msg is not None:
                    # Run single detection
                    t0 = time.time()
                    # grasps array contains the grasp poses, confidences, and widths.
                    # pc_confs array contains the colormapped pointcloud for visualization
                    grasps_array, confidences_array = self.grasp_predictor.detect(
                        self._current_pc
                    )
                    t1 = time.time()

                    # Publish results
                    if grasps_array is not None:
                        grasps_pc = array_to_pointcloud2(grasps_array)
                        grasps_pc.header = self._current_pc_msg.header
                        self.grasps_pc_pub.publish(grasps_pc)

                        if self.verbose:
                            rospy.loginfo(
                                f"Found {len(grasps_array)} grasps in {1000*(t1 - t0):.2f} ms",
                            )
                    if confidences_array is not None:
                        # Convert points arr from detector to Pointcloud2
                        confidences_pc = array_to_pointcloud2(confidences_array)
                        confidences_pc.header = self._current_pc_msg.header
                        self.conf_pc_pub.publish(confidences_pc)

                    else:
                        if self.verbose and self._has_new_pc_msg:
                            rospy.loginfo("No grasps detected!")

            self._has_new_pc = False
            self._rate.sleep()


if __name__ == "__main__":
    try:
        rospy.init_node("grasp_synthesis_node")
        rospy.loginfo("Starting Grasp Synthesis Node.")
        grasp_synth = GraspSynthesis()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass

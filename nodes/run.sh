#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

docker run --rm -it \
    --gpus all \
    -p 8888:8888 \
    -v $SCRIPT_DIR/../src:/grasp_synth/grasp_synth_ws/src/grasp_synthesis/src \
    -v $SCRIPT_DIR/../nodes/grasp_synthesis_node.py:/grasp_synth/grasp_synth_ws/src/grasp_synthesis/nodes/grasp_synthesis_node.py \
    grasp_synth:latest

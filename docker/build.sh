#!/bin/bash
# To run: docker run -it image_name /bin/bash
# docker tag raven_manip_sw:latest registry.gitlab.com/apl-ocean-engineering/raven/manipulation/object_detection/object_detection:TAG

# Build Target:
TARGET="node"

while [[ "$#" -gt 0 ]]; do
    case $1 in
        --target) TARGET="$2"; shift ;;
        *) echo "Unknown option: $1" ;;
    esac
    shift
done

# Build the Docker image with the specified target and tag
cd ../../ && \
    DOCKER_BUILDKIT=1 docker build --progress=plain . \
        --file grasp_synthesis/docker/Dockerfile \
        --target "$TARGET" \
        --tag "grasp_synthesis:$TARGET"

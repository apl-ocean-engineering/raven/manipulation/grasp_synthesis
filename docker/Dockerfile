FROM micat001/grasp_synthesis:latest AS base
ENV WORKDIR=/grasp_synth
WORKDIR $WORKDIR

# copy this package into the image
RUN mkdir -p /grasp_synth/grasp_synth_ws/src
WORKDIR $WORKDIR/WORKDIR_ws/src
RUN git clone https://gitlab.com/apl-ocean-engineering/raven/manipulation/grasp_synthesis.git && \
    vcs import < grasp_synthesis/grasp_synthesis.repos -w 1 --recursive


# build our package
WORKDIR $WORKDIR/grasp_synth_ws
RUN . /opt/ros/noetic/setup.sh \
    && catkin build -DPYTHON_EXECUTABLE=/usr/bin/python3

ENV NN_CONDA_PATH=/grasp_synth/miniconda3/envs/tsgrasp/bin/python
ENV OMP_NUM_THREADS=16

# Source the workspace on entry
RUN echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
RUN echo "source $WORKDIR/grasp_synth_ws/devel/setup.bash" >> ~/.bashrc

# Add pretty colors to bash
RUN echo 'export PS1="\[\033[01;32m\]\u@\h:\[\033[01;34m\]\w\[\033[00m\]\$ "' >> ~/.bashrc && \
    echo 'alias ls="ls --color=auto"' >> ~/.bashrc && \
    echo 'export CLICOLOR=1' >> ~/.bashrc && \
    echo 'export LSCOLORS=GxFxCxDxBxegedabagaced' >> ~/.bashrc

FROM base AS node

# Run the node on entry
CMD ["/bin/bash", "-c", "source /grasp_synth/grasp_synth_ws/devel/setup.bash && \
    rosrun --prefix $NN_CONDA_PATH grasp_synthesis grasp_synthesis_node.py"]
